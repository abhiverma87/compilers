open LibUtil

(* 
 * Parse and AST from a lexbuf 
 * - the filename is used to generate error messages
 *)
let parse (filename : string) (buf : Lexing.lexbuf) : Ast.prog =
  try
    Lexer.reset_lexbuf filename buf;
    Parser.toplevel Lexer.token buf
  with Parsing.Parse_error ->
    failwithf  "Parse error at %s." (Range.string_of_range (Lexer.lex_range buf))

let f_rt f l b =
  let rev_l = List.rev l
  in 
  let rec fold_r acc = function
    | [] -> acc
    | hd::tl -> fold_r (f hd acc) tl
  in 
  fold_r b rev_l

(* 
 * Compile a source binop in to an LL instruction.
 *)
let compile_binop (b : Ast.binop) : Ll.uid -> Ll.operand -> Ll.operand -> Ll.insn  =
  let ib b id op1 op2 = (Ll.Binop (id, b, op1, op2)) in
  let ic c id op1 op2 = (Ll.Icmp (id, c, op1, op2)) in
  match b with
  | Ast.Plus  -> ib Ll.Add
  | Ast.Times -> ib Ll.Mul
  | Ast.Minus -> ib Ll.Sub
  | Ast.And   -> ib Ll.And
  | Ast.Or    -> ib Ll.Or
  | Ast.Shl   -> ib Ll.Shl
  | Ast.Shr   -> ib Ll.Lshr
  | Ast.Sar   -> ib Ll.Ashr

  | Ast.Eq    -> ic Ll.Eq
  | Ast.Neq   -> ic Ll.Ne
  | Ast.Lt    -> ic Ll.Slt
  | Ast.Lte   -> ic Ll.Sle
  | Ast.Gt    -> ic Ll.Sgt
  | Ast.Gte   -> ic Ll.Sge

let compile_unop (u : Ast.unop) (id: Ll.uid) (op : Ll.operand) : Ll.insn =
	begin match u with
	| Ast.Neg    -> (Ll.Binop (id, Ll.Mul, op, Ll.Const (-1l)))
	| Ast.Lognot -> (Ll.Icmp  (id, Ll.Eq,  op, Ll.Const (0l )))
	| Ast.Not 	 -> (Ll.Binop (id, Ll.Xor, op, Ll.Const (-1l)))
	end
	
let compile_opnd (e : Ast.exp) (c : Ctxt.t) : Ll.operand option =
	begin match e with
	| Ast.Id s -> Some (Ll.Local (Ctxt.lookup s c))
	| _ -> None
	end

exception Operand_error of string

let rec compile_exp (e : Ast.exp) (c : Ctxt.t) : Ll.uid * Ll.insn list = 
	let var = Ll.gen_sym() in 
	begin match e with 
	| Ast.Cint i -> (var, [Ll.Binop (var, Ll.Mul, Ll.Const i, Ll.Const 1l)])
	| Ast.Id x ->  let operand = compile_opnd e c in 
			begin match operand with
			| Some op -> (var, [Ll.Load (var, op)] )
			| None -> raise (Operand_error "Operand Not Found")
			end 
	| Ast.Binop (op, e1, e2) -> 
			begin 
				let temp_uid1, e1_insns = compile_exp e1 c in
				let e1_op = Ll.Local temp_uid1 in 
				let temp_uid2, e2_insns = compile_exp e2 c in
				let e2_op = Ll.Local temp_uid2 in 
				let binop_insn = compile_binop op var e1_op e2_op in
				(var, e1_insns @ e2_insns @ [binop_insn]) 
			end
	| Ast.Unop (op,ex) -> 
			begin
				let temp_uid,ex_insns = compile_exp ex c in
				let ex_op = Ll.Local temp_uid in
				let unop_insn = compile_unop op var ex_op in
				(var, ex_insns @ [unop_insn])
			end
	end
	

let compile_var_decl ((insns, prev_ctxt) : Ll.insn list * Ctxt.t ) (var : Ast.var_decl) : Ll.insn list * Ctxt.t = 
	let new_ctxt, temp_uid = Ctxt.alloc var.Ast.v_id prev_ctxt in
	let eval_uid, eval_insn = compile_exp var.Ast.v_init prev_ctxt in
	let oprnd = Ll.Local eval_uid in
	let instructions : Ll.insn list = [Ll.Alloca (temp_uid)]  @ [Ll.Store (oprnd, Ll.Local (temp_uid))] @ [] in
	let ll_insns = insns @ eval_insn @ instructions in 
	(ll_insns,new_ctxt)
	
let rec compile_var_decls (vars : Ast.var_decl list) (prev_ctxt : Ctxt.t) =
	List.fold_left compile_var_decl ([], prev_ctxt) vars
	
	
let compile_lhs (l : Ast.lhs) (c:Ctxt.t) : Ll.uid = 
	begin match l with 
	| Ast.Var x -> (Ctxt.lookup x c)
	end
	

let rec compile_stmt (s : Ast.stmt) (c : Ctxt.t) (last_lbl : Ll.lbl) : Ll.lbl * Ll.bblock list =
	begin match s with
  	| Ast.Assign (l, e) -> 
		begin
			let temp_uid = Ll.Local (compile_lhs l c) in
			let e_uid, tmp_insns = compile_exp e c in
			let e_op = Ll.Local e_uid in
			let e_insns = tmp_insns @ [Ll.Store (e_op, temp_uid)] in
			let tmp_label = Ll.mk_lbl () in
			let a_blk = {Ll.label = tmp_label; Ll.insns = e_insns; Ll.terminator = Ll.Br last_lbl} in
			(tmp_label,[a_blk])
		end
			
  	| Ast.If (e, stmt1, stmt2) ->
		begin
			let e_uid,tmp_insns = compile_exp e c in
			let e_op = Ll.Local e_uid in 
			let t1_lbl,then_blk = compile_stmt stmt1 c last_lbl in 
			(*else condition is optional*)
			let t2_lbl,else_blk = 
				begin match stmt2 with
				| Some st -> compile_stmt st c last_lbl
				| None -> let x = Ll.mk_lbl () in (x,[{Ll.label = x; Ll.insns = []; Ll.terminator = Ll.Br last_lbl}])
				end in
			let this_blk = {Ll.label  = Ll.mk_lbl () ; 
				Ll.insns = tmp_insns; Ll.terminator = Ll.Cbr (e_op, (List.hd then_blk).Ll.label, (List.hd else_blk).Ll.label)} in
			((this_blk).Ll.label,[this_blk] @ then_blk @ else_blk)
		end
			
  	| Ast.While (e, s) ->
		begin
			let e_uid,tmp_insns = compile_exp e c in
			let e_op = Ll.Local e_uid in 
			let this_lbl = Ll.mk_lbl ()  in 
			let then_lbl,then_blks = compile_stmt s c this_lbl in 
			let this_blk = {Ll.label  = this_lbl; 
				Ll.insns = tmp_insns; 
				Ll.terminator = Ll.Cbr (e_op, (List.hd then_blks).Ll.label, last_lbl)}
			in (this_blk.Ll.label,[this_blk] @ then_blks)
		end		
		
  	| Ast.For (vdecs, e, s1, s2) ->
			begin
			let vdecs_insns, vnew_ctxt = compile_var_decls vdecs c in
			let vdec_lbl = Ll.mk_lbl () in 
			let e_lbl = Ll.mk_lbl () in
			let vdec_blk = {Ll.label = vdec_lbl;
					Ll.insns = vdecs_insns;
					Ll.terminator = Ll.Br e_lbl} in
					
			let inc_lbl, incr_block = 
				begin match s1 with
				| Some s -> compile_stmt s vnew_ctxt e_lbl
				| None -> let x_lbl = Ll.mk_lbl() in (x_lbl,[{Ll.label = x_lbl;
						Ll.insns = [];
						Ll.terminator = Ll.Br e_lbl}])
				end in
				
			let loop_lbl, loop_blk = compile_stmt s2 vnew_ctxt inc_lbl in 
			
			let e_blk = 
				begin match e with 
				| Some ex -> 
					begin
						let ex_uid, ex_insns = compile_exp ex vnew_ctxt in
						let ex_op = Ll.Local ex_uid in
						{Ll.label = e_lbl;
						Ll.insns = ex_insns;
						Ll.terminator = Ll.Cbr (ex_op, (List.hd loop_blk).Ll.label, last_lbl)}
					end	
				| None -> {Ll.label = e_lbl;
						Ll.insns =[];
						Ll.terminator = Ll.Br (List.hd loop_blk).Ll.label}
				end 
			in (vdec_lbl,[vdec_blk] @ [e_blk] @ loop_blk @ incr_block)
		end
			
  	| Ast.Block b -> 
			let blk_list, blk_ctxt = compile_block b c last_lbl in
			((List.hd blk_list).Ll.label,blk_list)
  	end
	
	
and compile_stmts (s : Ast.stmt list) (c : Ctxt.t) (next : Ll.lbl) : Ll.lbl * Ll.bblock list =
		let result = 
  		f_rt (
  			fun accum (blocks, next_lbl) ->
  				let s_lbl, s_blk = compile_stmt accum c next_lbl in
  				(s_blk @ blocks, s_lbl)
  				) s ([], next) 
		in (snd(result),fst(result))
		
and compile_block ((vars, stmts) : Ast.block) (c : Ctxt.t) (last_lbl : Ll.lbl)  : Ll.bblock list * Ctxt.t =
		let var_insns, new_ctxt = compile_var_decls vars c in
		let tmp_lbl, blk_list = compile_stmts stmts new_ctxt last_lbl in
		let main_block =
			{Ll.label =  Ll.mk_lbl ();
			 Ll.insns = var_insns;
			 Ll.terminator = Ll.Br tmp_lbl} in
		([main_block] @ blk_list, new_ctxt)

let compile_prog ((block, ret):Ast.prog) : Ll.prog =
	let last_lbl = Ll.mk_lbl () in
	let blk_list, blk_ctxt = compile_block block Ctxt.empty last_lbl in
	let last_uid, last_insns =  compile_exp ret blk_ctxt in
	let last_op = Ll.Local last_uid in
	let end_blk = { Ll.label = last_lbl;
				Ll.insns = last_insns;
				Ll.terminator = Ll.Ret (last_op) } in
	{Ll.ll_cfg = blk_list @ [end_blk] ; Ll.ll_entry = (List.hd blk_list).Ll.label }
	
	
	
	
	