open Ll
open X86simplified
open LibUtil

let emit_binop (b : Ll.bop) : X86simplified.insn =
	begin match b with
		| Ll.Add  -> Add (ecx,eax)
		| Ll.Sub  -> Sub (ecx,eax)
		| Ll.Mul  -> Imul(ecx,Eax)
		| Ll.Shl  -> Shl (ecx,eax)
		| Ll.Lshr -> Shr (ecx,eax)
		| Ll.Ashr -> Sar (ecx,eax)
		| Ll.And  -> And (ecx,eax)
		| Ll.Or   -> Or  (ecx,eax)
		| Ll.Xor  -> Xor (ecx,eax)
	end
        
let emit_icmp (i : Ll.cmpop) : X86simplified.ccode = 
	begin match i with
		| Ll.Eq   -> Eq
		| Ll.Ne   -> NotEq
		| Ll.Slt   -> Slt
		| Ll.Sle   -> Sle
		| Ll.Sgt   -> Sgt
		| Ll.Sge   -> Sge
	end 
	
let compile_blocks (blk_list : Ll.bblock list) : Cunit.cunit =
failwith "unimplemented"
	(*begin match blk_list with
	| 
	end *)
 

let compile_prog (prog : Ll.prog) : Cunit.cunit =
  let block_name = (Platform.decorate_cdecl "program") in
    let bblocklist = prog.Ll.ll_cfg in
    compile_blocks bblocklist 

