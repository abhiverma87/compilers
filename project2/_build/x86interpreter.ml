(* CS515: Project 2 X86Simplified Programming and X86Simplified Interpreter *)

open X86simplified

exception X86_segmentation_fault of string

(* Registers are interpreted as indices into register array *)
let ieax = 0
let iebx = 1
let iecx = 2
let iedx = 3
let iesi = 4
let iedi = 5
let iebp = 6
let iesp = 7

let get_register_id = function
 | Eax -> ieax
 | Ebx -> iebx
 | Ecx -> iecx
 | Edx -> iedx
 | Esi -> iesi
 | Edi -> iedi
 | Ebp -> iebp
 | Esp -> iesp


let mem_size = 1024                (* Size of memory in words *)
let mem_top : int32 = 0xfffffffcl  (* Last addressable memory location *)
let mem_bot: int32 = (Int32.mul (Int32.of_int (mem_size * 4)) (-1l))

(* Maps virtual addresses (int32 addresses) to physical addresses (int
 indices). Raises an X86_segmentation_fault exception if the provided
 virtual address does not map or if the address is unaligned.
*)
let mem_top_int = -1* Int32.to_int mem_top
let mem_bot_int = -1* Int32.to_int mem_bot

let map_addr (addr: int32) : int = 
	let x:int = -1 * Int32.to_int addr in 
	let err = "Address is unaligned or not within the 1024-words of the
    interpreter's simulated address space" in
	if (x<mem_top_int) || (x>mem_bot_int) || (x  mod 4 <> 0)
	then
		raise (X86_segmentation_fault err)
	else
		(mem_bot_int-x)/4

type x86_state = {
   s_memory : int32 array;  (* 1024 32-bit words -- the heap *)
   s_regs : int32 array;    (* 8 32-bit words -- the register file *)
   mutable s_of : bool;     (* overflow flag *)
   mutable s_sf : bool;     (* sign bit flag *)
   mutable s_zf : bool;     (* zero flag *)
}

let mk_init_state (): x86_state = 
  let xs = {
    s_memory = Array.make mem_size 0l;
    s_regs   = Array.make 8 0l;
    s_of     = false;
    s_sf     = false;
    s_zf     = false;
  } in 
  xs.s_regs.(iesp) <- mem_top; xs

let print_state (xs:x86_state): unit = 
 (Array.iter (fun e -> Printf.printf "%lx" e) xs.s_memory);
 (Printf.printf "\neax: %lx ebx: %lx ecx: %lx edx: %lx" xs.s_regs.(ieax) 
     xs.s_regs.(iebx) xs.s_regs.(iecx) xs.s_regs.(iedx));
 (Printf.printf "\nesi: %lx edi: %lx ebp: %lx esp: %lx" xs.s_regs.(iesi) 
     xs.s_regs.(iedi) xs.s_regs.(iebp) xs.s_regs.(iesp));
 (Printf.printf "\n OF: %b SF: %b ZF: %b" xs.s_of xs.s_sf xs.s_zf)


(* Helper function that determines whether a given condition code
   applies in the x86 state xs. *)  
let condition_matches (xs:x86_state) (c:X86simplified.ccode) : bool =
	begin match c with 
	| Eq -> xs.s_zf
	| Zero -> xs.s_zf
	| NotEq -> not xs.s_zf
	| NotZero -> not xs.s_zf
	| Slt -> xs.s_sf <> xs.s_of 
	| Sge -> xs.s_of = xs.s_sf
	| Sle -> (xs.s_sf <> xs.s_of) || xs.s_zf 
	| Sgt -> not (xs.s_zf || (xs.s_sf <> xs.s_of))
	end

(* Returns the bit at a given index in a 32-bit word as a boolean *)
let get_bit bitidx n =
  let shb = Int32.shift_left 1l bitidx in
  Int32.logand shb n = shb

(*Returns the actual memory location in case of indirect displacement
as base+(index*scale)+displacement*)
let indirect_index (xs:x86_state) (i:ind) : int32  =
	let base = 
		begin match i.i_base with
		| Some r -> xs.s_regs.(get_register_id r)
		| None -> 0l
		end
	and scale =
		begin match i.i_iscl with
		| Some (r,d) -> if(get_register_id r=iesp) then raise (X86_segmentation_fault "Register could not be Esp register") else Int32.mul (xs.s_regs.(get_register_id r)) d
		| None -> 0l
		end
	and disp =
		begin match i.i_disp with
		| Some DImm dim-> dim
		| Some DLbl lbl -> raise (X86_segmentation_fault "Cannot evaluate label for displacement")
		| None -> 0l
		end	
	in Int32.add base (Int32.add scale disp)

(*Returns the value of operand*)
let get_operand_value (xs:x86_state) (op:operand) : int32 = 
	begin match op with
	| Imm v -> v
	| Lbl lbl -> raise (X86_segmentation_fault "Cannot evaluate label for value")
	| Reg r -> xs.s_regs.(get_register_id r)
	| Ind ind -> xs.s_memory.(map_addr (indirect_index xs ind))
	end
	
(*Sets the value of an operand in x86_state and returns the state*)
let set_operand_value (xs:x86_state) (op:operand) (v:int32) : (x86_state) = 
	begin match op with
	| Imm i -> raise (X86_segmentation_fault "Cannot set value for an Immediate value")
	| Lbl lbl -> raise (X86_segmentation_fault "Cannot set value for label")
	| Reg r -> xs.s_regs.(get_register_id r) <- v; xs
	| Ind ind -> xs.s_memory.(map_addr (indirect_index xs ind)) <- v; xs
	end
	
(*Sets the values of various flags in x86_state and returns the state*)
let setFlags (xs:x86_state) (v:int32) (b:bool) : (x86_state) =
begin
	xs.s_zf <- v=0l;
	xs.s_sf <- get_bit 31 v;
	xs.s_of <- b;
	xs
end

(*get_bit for 64 bit numbers*)
let get_bit64 bitidx64 n =
  let shb = Int64.shift_left (Int64.of_int 1) bitidx64 in
  Int64.logand shb n = shb
  
  (*ARITHMETIC OPERATIONS*)

(*Performs the Neg(dest) operation *)
let neg_impl (xs:x86_state) (dest:operand) : x86_state = 
begin
	let temp:int32 = get_operand_value xs dest in
	let negVal:int32 = Int32.neg temp in
	let xs':x86_state = 
		begin if (temp=Int32.min_int) 
			then
				setFlags xs negVal true
			else
				setFlags xs negVal false
		end
	in set_operand_value xs' dest negVal
end

let add_impl (xs:x86_state) (src:operand) (dest:operand) : x86_state =
begin
	let dest64:int64 = Int64.of_int32 (get_operand_value xs dest) in
	let src64:int64 = Int64.of_int32 (get_operand_value xs src) in
	let r64:int64 = Int64.add dest64 src64 in
	(*let r32:int32 = Int32.add (get_operand_value xs src) (get_operand_value xs dest) in *)
	let r32:int32 = Int64.to_int32 r64 in
	let xs':x86_state = 
		begin if ((get_bit64 63 src64)=(get_bit64 63 dest64)) && ((get_bit64 63 src64)<>(get_bit 31 r32))
		then 
			setFlags xs r32 true
		else
			setFlags xs r32 false
		end
	in set_operand_value xs' dest r32
end

let sub_impl (xs:x86_state) (src:operand) (dest:operand) : x86_state =
begin
	let dest64:int64 = Int64.of_int32 (get_operand_value xs dest) in
	let src64:int64 = Int64.of_int32 (get_operand_value xs src) in
	let r64:int64 = Int64.add dest64 (Int64.neg src64) in
	(*let r32:int32 = Int32.sub (get_operand_value xs src) (get_operand_value xs dest) in *)
	let r32:int32 = Int64.to_int32 r64 in
	let xs':x86_state = 
		begin if (((get_bit64 63 (Int64.neg src64))=(get_bit64 63 dest64)) && ((get_bit64 63 (Int64.neg src64))<>(get_bit 31 r32))) || ((get_operand_value xs src) = Int32.min_int)
		then 
			setFlags xs r32 true
		else
			setFlags xs r32 false
		end
	in set_operand_value xs' dest r32
end

let imul_impl (xs:x86_state) (src:operand) (dest:reg) : x86_state =
begin
	let dest64:int64 = Int64.of_int32 (xs.s_regs.(get_register_id dest)) in
	let src64:int64 = Int64.of_int32 (get_operand_value xs src) in
	(*let r32:int32 = Int32.mul (get_operand_value xs src) (get_operand_value xs dest) in *)
	let r64:int64 = Int64.mul dest64 src64 in
	let r32:int32 = Int64.to_int32 r64 in
	let xs':x86_state = 
		begin if (Int64.of_int32 r32<>r64)
		then 
			setFlags xs r32 true
		else
			setFlags xs r32 false
		end
	in xs'.s_regs.(get_register_id dest) <- r32;
	xs'
end

(*LOGICAL OPERATIONS*)

let not_impl (xs:x86_state) (dest:operand) : x86_state = 
begin
	let notVal:int32 = Int32.lognot (get_operand_value xs dest) in
	set_operand_value xs dest notVal
end

let and_impl (xs:x86_state) (src:operand) (dest:operand) : x86_state =
begin
	let andVal:int32 = Int32.logand (get_operand_value xs dest) (get_operand_value xs src) in
	let xs':x86_state = 
	begin
		setFlags xs andVal false
	end
	in set_operand_value xs' dest andVal
end

let or_impl (xs:x86_state) (src:operand) (dest:operand) : x86_state =
begin
	let andVal:int32 = Int32.logor (get_operand_value xs dest) (get_operand_value xs src) in
	let xs':x86_state = 
	begin
		setFlags xs andVal false
	end in
	set_operand_value xs' dest andVal
end

let xor_impl (xs:x86_state) (src:operand) (dest:operand) : x86_state =
begin
	let andVal:int32 = Int32.logxor (get_operand_value xs dest) (get_operand_value xs src) in
	let xs':x86_state = 
	begin
		setFlags xs andVal false
	end in
	set_operand_value xs' dest andVal
end

(*BIT MANIPULATION OPERATIONS*)

let sar_impl (xs:x86_state) (amt:operand) (dest:operand) : x86_state = 
begin
	let amtVal = 
	begin match amt with
		| Imm i -> get_operand_value xs amt
		| Reg r -> if (get_register_id r)=iecx 
				then
					get_operand_value xs amt
				else
					raise (X86_segmentation_fault "Register should be ECX")
		| _ -> raise (X86_segmentation_fault "AMT must be a Imm or ECX operand")
	end in
	let shiftedVal = Int32.shift_right (get_operand_value xs dest) (Int32.to_int amtVal) in
	let xs':x86_state = 
	begin
		if(Int32.to_int amtVal <> 0)
		then
			begin
			if(Int32.to_int amtVal = 1)
			then 
				setFlags xs shiftedVal false
			else
				begin
				xs.s_zf <- shiftedVal=0l;
				xs.s_sf <- get_bit 31 shiftedVal;
				xs
				end
			end
		else
			xs
	end
	in set_operand_value xs' dest shiftedVal
end

let shl_impl (xs:x86_state) (amt:operand) (dest:operand) : x86_state = 
begin
	let destVal = get_operand_value xs dest in
	let amtVal = 
	begin match amt with
		| Imm i -> get_operand_value xs amt
		| Reg r -> if (get_register_id r)=iecx
				then
					get_operand_value xs amt
				else
					raise (X86_segmentation_fault "Register should be ECX")
		| _ -> raise (X86_segmentation_fault "AMT must be a Imm or ECX operand")
	end in
	let shiftedVal = Int32.shift_left (get_operand_value xs dest) (Int32.to_int amtVal) in
	let xs':x86_state = 
	begin
		if(Int32.to_int amtVal <> 0)
		then
			begin
			if (Int32.to_int amtVal = 1) && (get_bit 31 destVal <> get_bit 30 destVal)
			then 
				setFlags xs shiftedVal true
			else
				begin
				xs.s_zf <- shiftedVal=0l;
				xs.s_sf <- get_bit 31 shiftedVal;
				xs
				end
			end
		else
			xs
	end
	in set_operand_value xs' dest shiftedVal
end

let shr_impl (xs:x86_state) (amt:operand) (dest:operand) : x86_state = 
begin
	let destVal = get_operand_value xs dest in
	let amtVal = 
	begin match amt with
		| Imm i -> get_operand_value xs amt
		| Reg r -> if (get_register_id r)=iecx
				then
					get_operand_value xs amt
				else
					raise (X86_segmentation_fault "Register should be ECX")
		| _ -> raise (X86_segmentation_fault "AMT must be a Imm or ECX operand")
	end in
	let shiftedVal = Int32.shift_right_logical (get_operand_value xs dest) (Int32.to_int amtVal) in
	let xs':x86_state = 
	begin
		if(Int32.to_int amtVal <> 0)
		then
			begin
			if (Int32.to_int amtVal = 1)
			then 
				setFlags xs destVal (get_bit 31 destVal)
			else
				xs
			end
		else
			xs
	end
	in set_operand_value xs' dest shiftedVal
end

let setb_impl (xs:x86_state) (cc:X86simplified.ccode) (dest:operand) : x86_state = 
let destVal = get_operand_value xs dest in
begin
	if (condition_matches xs cc) 
	then
		set_operand_value xs dest (Int32.logor destVal 1l)
	else 
		if (get_bit 0 destVal)
		then
		set_operand_value xs dest (Int32.logxor destVal 1l)	
	else
		xs
		
end

(* DATA MOVEMENT INSTRUCTIONS  *)
let lea_impl (xs:x86_state) (i:ind) (dest:reg) : x86_state= 
begin
	let ind_index:int32  = indirect_index xs i in
	xs.s_regs.(get_register_id dest) <- ind_index;
	xs
end

let mov_impl (xs:x86_state) (src:operand) (dest:operand) : x86_state =
begin
	set_operand_value xs dest (get_operand_value xs src)
end

let push_impl (xs:x86_state) (src:operand) : x86_state =
begin
	(*let esp_val:int32 = Int32.sub (xs.s_regs.(iesp)) 4l in*)
	xs.s_regs.(iesp) <- Int32.sub (xs.s_regs.(iesp)) 4l ;
	xs.s_memory.(map_addr (xs.s_regs.(iesp))) <- get_operand_value xs src;
	xs
end

let pop_impl (xs:x86_state) (dest:operand) : x86_state =
begin
	let op = xs.s_memory.(map_addr (xs.s_regs.(iesp))) in
	xs.s_regs.(iesp) <- Int32.add (xs.s_regs.(iesp)) 4l ;
	set_operand_value xs dest op
end

(*CONTROL-FLOW RELATED OPERATIONS*)
let cmp_impl (xs:x86_state) (src2:operand) (src1:operand) : x86_state = 
begin
	let src2_64:int64 = Int64.of_int32 (get_operand_value xs src2) in
	let src1_64:int64 = Int64.of_int32 (get_operand_value xs src1) in
	let r64:int64 = Int64.add src1_64 (Int64.neg src2_64) in
	let r32:int32 = Int64.to_int32 r64 in
	begin if (((get_bit64 63 (Int64.neg src2_64))=(get_bit64 63 src1_64)) && ((get_bit64 63 (Int64.neg src2_64))<>(get_bit 31 r32))) || ((get_operand_value xs src2) = Int32.min_int)
	then 
		setFlags xs r32 true
	else
		setFlags xs r32 false
	end
end

(*JUMP/CALL/J AND RETURN ARE IMPLEMENTED IN interpret_insn method*)


module LblMap = Map.Make(struct type t = lbl let compare = compare end)

let rec lbl_map (code:insn_block list) : insn_block LblMap.t =
  begin match code with
  | []   -> LblMap.empty
  | h::t -> LblMap.add (h.label) h (lbl_map t)
  end

let get_label (o:operand) : lbl =
begin match o with
  | Lbl l -> l
  | _ -> raise (X86_segmentation_fault "Operand should be a label")
end

let rec interpret_insn (xs:x86_state) (lbl_map:insn_block LblMap.t)  (ins:insn) : x86_state= 
	begin match ins with
			| Neg op -> neg_impl xs op
			| Add (op1 , op2) -> add_impl xs op1 op2
			| Sub (op1,op2) -> sub_impl xs op1 op2
			| Imul (op1,op2) -> imul_impl xs op1 op2
			| Not op -> not_impl xs op
			| And (op1,op2) -> and_impl xs op1 op2
			| Or (op1,op2) -> or_impl xs op1 op2
			| Xor(op1,op2) -> xor_impl xs op1 op2
			| Sar (amt,op) -> sar_impl xs amt op
			| Shl (amt,op) -> shl_impl xs amt op
			| Shr (amt,op) -> shr_impl xs amt op
			| Setb (cc,op) -> setb_impl xs cc op
			| Lea (ind,reg) -> lea_impl xs ind reg
			| Mov (src,dest) -> mov_impl xs src dest
			| Push op -> push_impl xs op
			| Pop op -> pop_impl xs op
			| Cmp (op1,op2) -> cmp_impl xs op1 op2
			| Jmp (s)      -> locate_block xs lbl_map (get_label s)
			| Call (s)     ->   let new_esp = Int32.sub (get_operand_value xs (Reg Esp)) 4l in
						let xs' = set_operand_value xs (Reg Esp) new_esp in
						locate_block xs' lbl_map (get_label s)
			| Ret  -> set_operand_value xs (Reg Esp) (Int32.add (get_operand_value xs (Reg Esp)) 4l)
			| J(cc, clbl) ->    if condition_matches xs cc then locate_block xs lbl_map clbl else xs
	  end

and interpret_code_block (xs:x86_state) (lbl_map:insn_block LblMap.t) (insns:insn list) : x86_state = 
begin match insns with
	  | []  -> raise (X86_segmentation_fault "No Code block present")
	  | [h] -> 
		begin match h with
			    | Jmp(s)  -> interpret_insn xs lbl_map h
			    | Ret -> interpret_insn xs lbl_map h
			    | J(c, l) -> interpret_insn xs lbl_map h
			    | _       -> raise (X86_segmentation_fault "Code block must end with Jmp, Ret, or J")
		end
	  | h::t ->
		begin match h with 
		| Jmp(_)  -> interpret_insn xs lbl_map h
		| J(c, _) -> if condition_matches xs c then
					interpret_insn xs lbl_map h
				else
					let xs' = interpret_insn xs lbl_map h
					in interpret_code_block xs' lbl_map t
		| _ -> let xs' = interpret_insn xs lbl_map h
			 in interpret_code_block xs' lbl_map t
		end
	  end

and locate_block (xs:x86_state) (lbl_map:insn_block LblMap.t) (l:lbl) : x86_state =
	  let n_blk = LblMap.find l lbl_map in 
	   interpret_code_block xs lbl_map n_blk.insns

let interpret (code:insn_block list) (xs:x86_state) (l:lbl) : unit =
  let map = lbl_map code in
  let _ = locate_block xs map l in ()


let run (code:insn_block list): int32 = 
  let main = mk_lbl_named "main" in 
  let xs = mk_init_state () in 
  let _ = interpret code xs main in 
    xs.s_regs.(ieax)
