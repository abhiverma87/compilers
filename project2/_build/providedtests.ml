open Assert
open X86interpreter
open X86simplified
open Gradedtests
(* These tests are provided by you -- they will be graded manually *)

(* You should also add additional test cases here to help you   *)
(* debug your program.                                          *)

let provided_tests : suite = [
  Test ("Student-Provided Big Test for Part II",[("Exponent", run_test 16l
[(mk_insn_block (mk_lbl_named "exponent") [
        Push (ebp);
	Mov (esp, ebp);
	Mov (ebx, stack_offset 12l);
	Mov (eax, stack_offset 8l);
	Cmp(Imm 0l,eax);
	J(Sgt, (mk_lbl_named "exponent_recurse"));
	Mov (Imm 1l, eax);
        Pop (ebp);
        Ret
    ]); 
(mk_insn_block (mk_lbl_named "exponent_recurse") [
      Sub (Imm 1l, eax);
      Push (ebx);
      Add ((Imm 4l), esp);
      Mov ((stack_offset 8l),eax);
      Imul (ebx,Ebx);
      Mov ((stack_offset 12l), ebx);
      Pop (ebp);
      Ret
    ]); 
(mk_insn_block (mk_lbl_named "main") [
      Push (Imm 2l);
      Push (Imm 4l);
      Call (Lbl (mk_lbl_named "exponent"));
      Add ((Imm 8l), esp);
      Ret;
   ])
])	
]);
]
  
  
 
