type token =
  | EOF
  | INT of (Range.t * int32)
  | X of (Range.t)
  | Plus of (Range.t)
  | Minus of (Range.t)
  | Times of (Range.t)
  | Eq of (Range.t)
  | Shl of (Range.t)
  | Sar of (Range.t)
  | Shr of (Range.t)
  | Neq of (Range.t)
  | Lt of (Range.t)
  | Lte of (Range.t)
  | Gt of (Range.t)
  | Gte of (Range.t)
  | Lognot of (Range.t)
  | Not of (Range.t)
  | And of (Range.t)
  | Or of (Range.t)
  | LPAREN of (Range.t)
  | RPAREN of (Range.t)

val toplevel :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Ast.exp
