type token =
  | EOF
  | INT of (Range.t * int32)
  | X of (Range.t)
  | Plus of (Range.t)
  | Minus of (Range.t)
  | Times of (Range.t)
  | Eq of (Range.t)
  | Shl of (Range.t)
  | Sar of (Range.t)
  | Shr of (Range.t)
  | Neq of (Range.t)
  | Lt of (Range.t)
  | Lte of (Range.t)
  | Gt of (Range.t)
  | Gte of (Range.t)
  | Lognot of (Range.t)
  | Not of (Range.t)
  | And of (Range.t)
  | Or of (Range.t)
  | LPAREN of (Range.t)
  | RPAREN of (Range.t)

open Parsing;;
let _ = parse_error;;
# 2 "parser.mly"
open Ast;;
# 29 "parser.ml"
let yytransl_const = [|
    0 (* EOF *);
    0|]

let yytransl_block = [|
  257 (* INT *);
  258 (* X *);
  259 (* Plus *);
  260 (* Minus *);
  261 (* Times *);
  262 (* Eq *);
  263 (* Shl *);
  264 (* Sar *);
  265 (* Shr *);
  266 (* Neq *);
  267 (* Lt *);
  268 (* Lte *);
  269 (* Gt *);
  270 (* Gte *);
  271 (* Lognot *);
  272 (* Not *);
  273 (* And *);
  274 (* Or *);
  275 (* LPAREN *);
  276 (* RPAREN *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\003\000\003\000\004\000\004\000\005\000\005\000\
\005\000\006\000\006\000\006\000\006\000\006\000\007\000\007\000\
\007\000\007\000\008\000\008\000\008\000\009\000\009\000\010\000\
\010\000\010\000\010\000\011\000\011\000\011\000\000\000"

let yylen = "\002\000\
\002\000\001\000\003\000\001\000\003\000\001\000\003\000\003\000\
\001\000\003\000\003\000\003\000\003\000\001\000\003\000\003\000\
\003\000\001\000\003\000\003\000\001\000\003\000\001\000\002\000\
\002\000\002\000\001\000\001\000\001\000\003\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\028\000\029\000\000\000\000\000\000\000\000\000\
\031\000\000\000\002\000\004\000\006\000\009\000\014\000\018\000\
\021\000\023\000\027\000\025\000\026\000\024\000\000\000\001\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\030\000\000\000\
\000\000\022\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000"

let yydgoto = "\002\000\
\009\000\010\000\011\000\012\000\013\000\014\000\015\000\016\000\
\017\000\018\000\019\000"

let yysindex = "\255\255\
\005\255\000\000\000\000\000\000\005\255\005\255\005\255\005\255\
\000\000\124\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\049\255\000\000\
\005\255\005\255\005\255\005\255\005\255\005\255\005\255\005\255\
\005\255\005\255\005\255\005\255\005\255\005\255\000\000\000\255\
\000\255\000\000\073\255\255\254\255\254\255\254\073\255\022\255\
\022\255\022\255\022\255\122\255\104\255"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\001\000\
\019\000\000\000\098\000\034\000\049\000\064\000\102\000\073\000\
\079\000\088\000\094\000\106\000\068\000"

let yygindex = "\000\000\
\000\000\010\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000"

let yytablesize = 398
let yytable = "\001\000\
\019\000\025\000\026\000\027\000\027\000\003\000\004\000\000\000\
\005\000\000\000\000\000\000\000\000\000\000\000\020\000\021\000\
\022\000\023\000\020\000\006\000\007\000\000\000\000\000\008\000\
\025\000\026\000\027\000\000\000\029\000\030\000\031\000\000\000\
\000\000\015\000\040\000\041\000\042\000\043\000\044\000\045\000\
\046\000\047\000\048\000\049\000\050\000\051\000\052\000\053\000\
\016\000\000\000\000\000\025\000\026\000\027\000\028\000\029\000\
\030\000\031\000\032\000\033\000\034\000\035\000\036\000\017\000\
\000\000\037\000\038\000\003\000\039\000\000\000\000\000\000\000\
\010\000\000\000\000\000\025\000\026\000\027\000\011\000\029\000\
\030\000\031\000\000\000\033\000\034\000\035\000\036\000\012\000\
\000\000\000\000\000\000\000\000\000\000\013\000\000\000\000\000\
\000\000\007\000\000\000\000\000\000\000\008\000\000\000\000\000\
\000\000\005\000\025\000\026\000\027\000\028\000\029\000\030\000\
\031\000\032\000\033\000\034\000\035\000\036\000\000\000\000\000\
\037\000\000\000\000\000\024\000\025\000\026\000\027\000\028\000\
\029\000\030\000\031\000\032\000\033\000\034\000\035\000\036\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\019\000\019\000\000\000\019\000\019\000\
\019\000\019\000\019\000\019\000\019\000\019\000\019\000\000\000\
\000\000\019\000\019\000\000\000\019\000\020\000\020\000\000\000\
\020\000\020\000\020\000\020\000\020\000\020\000\020\000\020\000\
\020\000\000\000\000\000\020\000\020\000\000\000\020\000\015\000\
\015\000\015\000\015\000\015\000\015\000\015\000\015\000\015\000\
\000\000\000\000\015\000\015\000\000\000\015\000\016\000\016\000\
\016\000\016\000\016\000\016\000\016\000\016\000\016\000\000\000\
\000\000\016\000\016\000\000\000\016\000\017\000\017\000\017\000\
\017\000\017\000\017\000\017\000\017\000\017\000\010\000\000\000\
\017\000\017\000\010\000\017\000\011\000\003\000\000\000\003\000\
\011\000\010\000\010\000\000\000\010\000\012\000\000\000\011\000\
\011\000\012\000\011\000\013\000\000\000\000\000\000\000\013\000\
\012\000\012\000\000\000\012\000\000\000\000\000\013\000\013\000\
\000\000\013\000\007\000\007\000\000\000\007\000\008\000\008\000\
\000\000\008\000\005\000\005\000\000\000\005\000\025\000\026\000\
\027\000\028\000\029\000\030\000\031\000\032\000\033\000\034\000\
\035\000\036\000\000\000\000\000\037\000\038\000"

let yycheck = "\001\000\
\000\000\003\001\004\001\005\001\005\001\001\001\002\001\255\255\
\004\001\255\255\255\255\255\255\255\255\255\255\005\000\006\000\
\007\000\008\000\000\000\015\001\016\001\255\255\255\255\019\001\
\003\001\004\001\005\001\255\255\007\001\008\001\009\001\255\255\
\255\255\000\000\025\000\026\000\027\000\028\000\029\000\030\000\
\031\000\032\000\033\000\034\000\035\000\036\000\037\000\038\000\
\000\000\255\255\255\255\003\001\004\001\005\001\006\001\007\001\
\008\001\009\001\010\001\011\001\012\001\013\001\014\001\000\000\
\255\255\017\001\018\001\000\000\020\001\255\255\255\255\255\255\
\000\000\255\255\255\255\003\001\004\001\005\001\000\000\007\001\
\008\001\009\001\255\255\011\001\012\001\013\001\014\001\000\000\
\255\255\255\255\255\255\255\255\255\255\000\000\255\255\255\255\
\255\255\000\000\255\255\255\255\255\255\000\000\255\255\255\255\
\255\255\000\000\003\001\004\001\005\001\006\001\007\001\008\001\
\009\001\010\001\011\001\012\001\013\001\014\001\255\255\255\255\
\017\001\255\255\255\255\000\000\003\001\004\001\005\001\006\001\
\007\001\008\001\009\001\010\001\011\001\012\001\013\001\014\001\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\003\001\004\001\255\255\006\001\007\001\
\008\001\009\001\010\001\011\001\012\001\013\001\014\001\255\255\
\255\255\017\001\018\001\255\255\020\001\003\001\004\001\255\255\
\006\001\007\001\008\001\009\001\010\001\011\001\012\001\013\001\
\014\001\255\255\255\255\017\001\018\001\255\255\020\001\006\001\
\007\001\008\001\009\001\010\001\011\001\012\001\013\001\014\001\
\255\255\255\255\017\001\018\001\255\255\020\001\006\001\007\001\
\008\001\009\001\010\001\011\001\012\001\013\001\014\001\255\255\
\255\255\017\001\018\001\255\255\020\001\006\001\007\001\008\001\
\009\001\010\001\011\001\012\001\013\001\014\001\006\001\255\255\
\017\001\018\001\010\001\020\001\006\001\018\001\255\255\020\001\
\010\001\017\001\018\001\255\255\020\001\006\001\255\255\017\001\
\018\001\010\001\020\001\006\001\255\255\255\255\255\255\010\001\
\017\001\018\001\255\255\020\001\255\255\255\255\017\001\018\001\
\255\255\020\001\017\001\018\001\255\255\020\001\017\001\018\001\
\255\255\020\001\017\001\018\001\255\255\020\001\003\001\004\001\
\005\001\006\001\007\001\008\001\009\001\010\001\011\001\012\001\
\013\001\014\001\255\255\255\255\017\001\018\001"

let yynames_const = "\
  EOF\000\
  "

let yynames_block = "\
  INT\000\
  X\000\
  Plus\000\
  Minus\000\
  Times\000\
  Eq\000\
  Shl\000\
  Sar\000\
  Shr\000\
  Neq\000\
  Lt\000\
  Lte\000\
  Gt\000\
  Gte\000\
  Lognot\000\
  Not\000\
  And\000\
  Or\000\
  LPAREN\000\
  RPAREN\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Ast.exp) in
    Obj.repr(
# 44 "parser.mly"
            ( _1 )
# 243 "parser.ml"
               : Ast.exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E1) in
    Obj.repr(
# 49 "parser.mly"
       (_1)
# 250 "parser.ml"
               : Ast.exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 51 "parser.mly"
               (Binop(Or,_1,_3))
# 259 "parser.ml"
               : 'E1))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E2) in
    Obj.repr(
# 52 "parser.mly"
       (_1)
# 266 "parser.ml"
               : 'E1))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 54 "parser.mly"
                (Binop(And,_1,_3))
# 275 "parser.ml"
               : 'E2))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E3) in
    Obj.repr(
# 55 "parser.mly"
       (_1)
# 282 "parser.ml"
               : 'E2))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 57 "parser.mly"
               (Binop(Eq,_1,_3))
# 291 "parser.ml"
               : 'E3))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 58 "parser.mly"
                (Binop(Neq,_1,_3))
# 300 "parser.ml"
               : 'E3))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E4) in
    Obj.repr(
# 59 "parser.mly"
       (_1)
# 307 "parser.ml"
               : 'E3))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 61 "parser.mly"
               (Binop(Lt,_1,_3))
# 316 "parser.ml"
               : 'E4))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 62 "parser.mly"
                (Binop(Lte,_1,_3))
# 325 "parser.ml"
               : 'E4))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 63 "parser.mly"
               (Binop(Gt,_1,_3))
# 334 "parser.ml"
               : 'E4))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 64 "parser.mly"
                (Binop(Gte,_1,_3))
# 343 "parser.ml"
               : 'E4))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E5) in
    Obj.repr(
# 65 "parser.mly"
       (_1)
# 350 "parser.ml"
               : 'E4))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 67 "parser.mly"
                (Binop(Shl,_1,_3))
# 359 "parser.ml"
               : 'E5))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 68 "parser.mly"
                (Binop(Sar,_1,_3))
# 368 "parser.ml"
               : 'E5))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 69 "parser.mly"
                (Binop(Shr,_1,_3))
# 377 "parser.ml"
               : 'E5))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E6) in
    Obj.repr(
# 70 "parser.mly"
       (_1)
# 384 "parser.ml"
               : 'E5))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 72 "parser.mly"
                 (Binop(Plus,_1,_3))
# 393 "parser.ml"
               : 'E6))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 73 "parser.mly"
                  (Binop(Minus,_1,_3))
# 402 "parser.ml"
               : 'E6))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E7) in
    Obj.repr(
# 74 "parser.mly"
       (_1)
# 409 "parser.ml"
               : 'E6))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Ast.exp) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 76 "parser.mly"
                  (Binop(Times,_1,_3))
# 418 "parser.ml"
               : 'E7))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E8) in
    Obj.repr(
# 77 "parser.mly"
       (_1)
# 425 "parser.ml"
               : 'E7))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 79 "parser.mly"
            (Unop(Not,_2))
# 433 "parser.ml"
               : 'E8))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 80 "parser.mly"
                        (Unop(Neg,_2))
# 441 "parser.ml"
               : 'E8))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Range.t) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : Ast.exp) in
    Obj.repr(
# 81 "parser.mly"
               (Unop(Lognot,_2))
# 449 "parser.ml"
               : 'E8))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'E9) in
    Obj.repr(
# 82 "parser.mly"
       (_1)
# 456 "parser.ml"
               : 'E8))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Range.t * int32) in
    Obj.repr(
# 84 "parser.mly"
        (Cint (snd(_1)))
# 463 "parser.ml"
               : 'E9))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Range.t) in
    Obj.repr(
# 85 "parser.mly"
        ( Arg )
# 470 "parser.ml"
               : 'E9))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : Range.t) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : Ast.exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : Range.t) in
    Obj.repr(
# 86 "parser.mly"
                      (_2)
# 479 "parser.ml"
               : 'E9))
(* Entry toplevel *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let toplevel (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Ast.exp)
