%{
open Ast;;
%}

/* Declare your tokens here. */
%token EOF
%token <Range.t * int32> INT
%token <Range.t> X        /* X */
%token <Range.t> Plus      /* + */ 
%token <Range.t> Minus      /* - */
%token <Range.t> Times       /* * */
%token <Range.t> Eq      /* == */
%token <Range.t> Shl      /* << */
%token <Range.t> Sar      /* >> */
%token <Range.t> Shr      /* >>> */
%token <Range.t> Neq     /* != */
%token <Range.t> Lt      /* < */
%token <Range.t> Lte      /* <= */
%token <Range.t> Gt      /* > */
%token <Range.t> Gte     /* >= */
%token <Range.t> Lognot      /* ! */
%token <Range.t> Not      /* ~ */
%token <Range.t> And      /* & */
%token <Range.t> Or      /* | */
%token <Range.t> LPAREN      /* ( */
%token <Range.t> RPAREN      /* ) */

/* ---------------------------------------------------------------------- */
%start toplevel
%type <Ast.exp> toplevel
%type <Ast.exp> exp

%left LPAREN RPAREN
%left Or
%left And
%nonassoc Eq Neq
%nonassoc Lt Lte Gt Gte
%left Shl Sar Shr 
%left Plus Minus
%left Times 
%right Not Lognot
%%
toplevel:
  | exp EOF { $1 }

/* Declare your productions here, starting with 'exp'. */

exp:
  | E1 {$1}
E1 : 
  | exp Or exp {Binop(Or,$1,$3)}
  | E2 {$1}
E2 :
  | exp And exp {Binop(And,$1,$3)}
  | E3 {$1}
E3 :
  | exp Eq exp {Binop(Eq,$1,$3)}
  | exp Neq exp {Binop(Neq,$1,$3)}
  | E4 {$1}
E4 :
  | exp Lt exp {Binop(Lt,$1,$3)}
  | exp Lte exp {Binop(Lte,$1,$3)}
  | exp Gt exp {Binop(Gt,$1,$3)}
  | exp Gte exp {Binop(Gte,$1,$3)}
  | E5 {$1}
E5 :
  | exp Shl exp {Binop(Shl,$1,$3)}
  | exp Sar exp {Binop(Sar,$1,$3)}
  | exp Shr exp {Binop(Shr,$1,$3)}
  | E6 {$1}
E6 :
  | exp Plus exp {Binop(Plus,$1,$3)}
  | exp Minus exp {Binop(Minus,$1,$3)}
  | E7 {$1}
E7 :
  | exp Times exp {Binop(Times,$1,$3)}
  | E8 {$1}
E8 :
  | Not exp {Unop(Not,$2)}
  | Minus exp %prec Not {Unop(Neg,$2)}
  | Lognot exp {Unop(Lognot,$2)}
  | E9 {$1}
E9 :
  | INT {Cint (snd($1))}
  | X   { Arg }
  | LPAREN exp RPAREN {$2}
