{
  open Lexing
  open Parser
  open Range
  
  exception Lexer_error of Range.t * string

  let pos_of_lexpos (p:Lexing.position) : pos =
    mk_pos (p.pos_lnum) (p.pos_cnum - p.pos_bol)
    
  let mk_lex_range (p1:Lexing.position) (p2:Lexing.position) : Range.t =
    mk_range p1.pos_fname (pos_of_lexpos p1) (pos_of_lexpos p2)

  let lex_range lexbuf : Range.t = mk_lex_range (lexeme_start_p lexbuf)
      (lexeme_end_p lexbuf)

  let reset_lexbuf (filename:string) lexbuf : unit =
    lexbuf.lex_curr_p <- {
      pos_fname = filename;
      pos_cnum = 0;
      pos_bol = 0;
      pos_lnum = 1;
    }
    
  (* Boilerplate to define exceptional cases in the lexer. *)
  let unexpected_char lexbuf (c:char) : 'a =
    raise (Lexer_error (lex_range lexbuf,
        Printf.sprintf "Unexpected character: '%c'" c))

}

(*http://www2.lib.uchicago.edu/keith/ocaml-class/exceptions.html  -> try/with block
Not sure when the try condition will be reached*)

(* Declare your aliases (let foo = regex) and rules here. *)
let whitespace = ['\t' ' ' '\r' '\n']
let digit = ['0'-'9']

rule token = parse
	  | eof { EOF }
	  | 'X' { X (lex_range lexbuf) }
	  | whitespace+ { token lexbuf }  (* skip whitespace *)
	  | digit+ { INT (lex_range lexbuf, try Int32.of_string(lexeme lexbuf) with int_of_string -> raise (Lexer_error (lex_range lexbuf,
      		Printf.sprintf "Incorrect number format")))}
	  | '+' { Plus (lex_range lexbuf) }
	  | '-' { Minus (lex_range lexbuf) }
	  | '*' { Times (lex_range lexbuf) }
          | "==" { Eq (lex_range lexbuf) }
	  | '<' { Lt (lex_range lexbuf) }
	  | "<=" { Lte (lex_range lexbuf) }
	  | '>' { Gt (lex_range lexbuf) }
	  | ">=" { Gte (lex_range lexbuf) }
	  | "<<" { Shl (lex_range lexbuf) }
	  | ">>" { Sar (lex_range lexbuf) }
	  | ">>>" { Shr (lex_range lexbuf) }
	  | "!=" { Neq (lex_range lexbuf) }
	  | '!' { Lognot (lex_range lexbuf) }
	  | '~' { Not (lex_range lexbuf) }
	  | '&' { And (lex_range lexbuf) }
	  | '|' { Or (lex_range lexbuf) }
	  | '(' { LPAREN (lex_range lexbuf) }
	  | ')' { RPAREN (lex_range lexbuf) }
	  | _ as c { unexpected_char lexbuf c }
