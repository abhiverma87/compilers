(* References :
http://www.cis.upenn.edu/~cis341/current/lectures/lec04.pdf
http://caml.inria.fr/pub/docs/manual-ocaml/lexyacc.html#sec287   .......... [ for help regarding precedence and associativity implementation ]
http://caml.inria.fr/pub/docs/manual-ocaml/libref/Lexing.html
*)


(* compiler.ml *)
(* A compiler for simple arithmetic expressions. *)

(******************************************************************************)

open Printf
open Ast
open X86simplified

(* Note that Ast has similarly named constructors that must be
              disambiguated.  For example: Ast.Shl vs. X86simplified.Shl *)

(* Parse an AST from a preexisting lexbuf. 
 * The filename is used to generate error messages.
*)
let parse (filename : string) (buf : Lexing.lexbuf) : exp =
  try
    Lexer.reset_lexbuf filename buf;
    Parser.toplevel Lexer.token buf
  with Parsing.Parse_error ->
    failwith (sprintf "Parse error at %s."
        (Range.string_of_range (Lexer.lex_range buf)))
	

(*Used registers Eax,Ecx and Edx for storing results as they are Caller Save.
Used Ebx to Pop and store values which will never be used*)

let rec emit_exp (e:exp) (stream : insn list) : insn list = 
 begin
        match e with 
		| Cint i -> Mov (Imm i, eax) :: stream
		| Arg -> Mov(edx,eax) :: stream
		| Binop (op,exp1,exp2) -> 
			let exp1insns : insn list = emit_exp exp1 []
			and exp2insns =  emit_exp exp2 [] in
			begin match op with
			| Plus ->  Add (Imm 4l,esp) :: ((Add(stack_offset 0l,eax) :: (exp2insns @ [Push eax] @ exp1insns @ stream)))
			| Minus -> Pop eax :: ((Sub(eax,stack_offset 0l) ::  (exp2insns @ [Push eax] @ exp1insns @ stream))) 
			| Times ->  Add (Imm 4l,esp) :: ((Imul(stack_offset 0l,Eax) :: (exp2insns @ [Push eax] @ exp1insns @ stream)))
			| Or ->  Add (Imm 4l,esp) :: ((Or(stack_offset 0l,eax) :: (exp2insns @ [Push eax] @ exp1insns @ stream)))
			| And ->  Add (Imm 4l,esp) ::  ((And(stack_offset 0l,eax) :: (exp2insns @ [Push eax] @ exp1insns @ stream)))
			| Eq -> Add (Imm 4l,esp) :: [Setb (Eq,eax) ] @ ((Cmp(stack_offset 0l,eax) :: (exp1insns @ [Push eax] @ exp2insns @ stream)))
			| Neq -> Add (Imm 4l,esp) :: [Setb (NotEq,eax) ] @ ((Cmp(stack_offset 0l,eax) :: (exp1insns @ [Push eax] @ exp2insns @ stream)))
			| Lt -> Add (Imm 4l,esp) :: [Setb (Slt,eax) ] @ ((Cmp(stack_offset 0l,eax) :: (exp1insns @ [Push eax] @ exp2insns @ stream)))
			| Lte ->  Add (Imm 4l,esp) :: [Setb (Sle,eax) ] @ ((Cmp(stack_offset 0l,eax) :: (exp1insns @ [Push eax] @ exp2insns @ stream)))
			| Gt ->  Add (Imm 4l,esp) :: [Setb (Sgt,eax) ] @ ((Cmp(stack_offset 0l,eax) :: (exp1insns @ [Push eax] @ exp2insns @ stream)))
			| Gte ->  Add (Imm 4l,esp) :: [Setb (Sge,eax) ] @ ((Cmp(stack_offset 0l,eax) :: (exp1insns @ [Push eax] @ exp2insns @ stream)))
			(*referred forum discussion for idea to implement shift operations using ecx as a temporary register
			https://sakai.rutgers.edu/portal/site/14746288-1528-4634-8bff-0d23e97ef725/page/718a3935-d60d-4089-85e9-cba0b619961d*)
			| Shl ->  Pop edx :: Mov (stack_offset 0l,eax) :: ((Shl(ecx,stack_offset 0l) :: ([Mov(eax,ecx)] @ exp2insns @ [Push eax] @ exp1insns @ stream)))
			| Shr ->  Pop edx :: Mov (stack_offset 0l,eax) :: ((Shr(ecx,stack_offset 0l) :: ([Mov(eax,ecx)] @ exp2insns @ [Push eax] @ exp1insns @ stream)))
			| Sar ->  Pop edx :: Mov (stack_offset 0l,eax) :: ((Sar(ecx,stack_offset 0l) :: ([Mov(eax,ecx)] @ exp2insns @ [Push eax] @ exp1insns @ stream)))
			end
		| Unop (op,ep) ->
			let ep_insns : insn list = emit_exp ep [] in
			begin match op with
			| Neg -> [Neg eax] @ ep_insns @ stream
			| Lognot -> [Setb(Eq,eax)] @ [Cmp(Imm 0l,eax) ] @ ep_insns @ stream
			| Not -> [Not eax] @ ep_insns @ stream
			end	
end
 

(* Builds a globally-visible X86 instruction block that acts like the C fuction:

   int program(int X) { return <expression>; }

   Follows cdecl calling conventions and platform-specific name mangling policy. *)

let compile_exp (ast:exp) : Cunit.cunit =
  let block_name = (Platform.decorate_cdecl "program") in
	let code : insn list = List.rev (Ret :: (emit_exp ast [Mov (stack_offset 4l ,edx)])) in
		let code_block : insn_block = {global = true; label = mk_lbl_named (block_name); insns = code} in
			let blockComp : Cunit.component = Cunit.Code code_block
				in [blockComp] 

