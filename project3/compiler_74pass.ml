(* compiler.ml *)
(* A compiler for simple arithmetic expressions. *)

(******************************************************************************)

open Printf
open Ast
open X86simplified

(* Note that Ast has similarly named constructors that must be
              disambiguated.  For example: Ast.Shl vs. X86simplified.Shl *)

(* Parse an AST from a preexisting lexbuf. 
 * The filename is used to generate error messages.
*)
let parse (filename : string) (buf : Lexing.lexbuf) : exp =
  try
    Lexer.reset_lexbuf filename buf;
    Parser.toplevel Lexer.token buf
  with Parsing.Parse_error ->
    failwith (sprintf "Parse error at %s."
        (Range.string_of_range (Lexer.lex_range buf)))

let rec emit_exp (e:exp) (stream : insn list) : insn list = 
 begin
        match e with 
		| Cint i -> Mov (Imm i, eax) :: stream
		| Arg -> Mov(ebx,eax) :: Mov (stack_offset 4l ,ebx) :: stream
		| Binop (op,exp1,exp2) -> 
			let exp1insns : insn list = emit_exp exp1 []
			and exp2insns =  emit_exp exp2 [] in
			begin match op with
			| Plus ->  ((Add(edx,eax) :: (exp2insns @ [Mov(eax,edx)] @ exp1insns @ stream)))
			| Minus -> Mov(edx,eax) :: ((Sub(eax,edx) :: (exp2insns @ [Mov(eax,edx)] @ exp1insns @ stream))) 
			| Times ->  ((Imul(edx,Eax) :: (exp2insns @ [Mov(eax,edx)] @ exp1insns @ stream)))
			| Or ->  ((Or(edx,eax) :: (exp2insns @ [Mov(eax,edx)] @ exp1insns @ stream)))
			| And ->  ((And(edx,eax) :: (exp2insns @ [Mov(eax,edx)] @ exp1insns @ stream)))
			| Eq -> [Setb (Eq,eax) ] @ ((Cmp(edx,eax) :: (exp1insns @ [Mov(eax,edx)] @ exp2insns @ stream)))
			| Neq -> [Setb (NotEq,eax) ] @ ((Cmp(edx,eax) :: (exp1insns @ [Mov(eax,edx)] @ exp2insns @ stream)))
			| Lt -> [Setb (Slt,eax) ] @ ((Cmp(edx,eax) :: (exp1insns @ [Mov(eax,edx)] @ exp2insns @ stream)))
			| Lte ->  [Setb (Sle,eax) ] @ ((Cmp(edx,eax) :: (exp1insns @ [Mov(eax,edx)] @ exp2insns @ stream)))
			| Gt ->  [Setb (Sgt,eax) ] @ ((Cmp(edx,eax) :: (exp1insns @ [Mov(eax,edx)] @ exp2insns @ stream)))
			| Gte ->  [Setb (Sge,eax) ] @ ((Cmp(edx,eax) :: (exp1insns @ [Mov(eax,edx)] @ exp2insns @ stream)))
			| Shl ->  ((Shl(ecx,eax) :: (exp1insns @ [Mov(eax,ecx)] @ exp2insns @ stream)))
			| Shr ->  ((Shr(ecx,eax) :: (exp1insns @ [Mov(eax,ecx)] @ exp2insns @ stream)))
			| Sar ->  ((Sar(ecx,eax) :: (exp1insns @ [Mov(eax,ecx)] @ exp2insns @ stream)))
			end
		| Unop (op,ep) ->
			let ep_insns : insn list = emit_exp ep [] in
			begin match op with
			| Neg -> [Neg eax] @ ep_insns @ stream
			| Lognot -> [Setb(Eq,eax)] @ [Cmp(Imm 0l,eax) ] @ ep_insns @ stream
			| Not -> [Not eax] @ ep_insns @ stream
			end	
end
 

(* Builds a globally-visible X86 instruction block that acts like the C fuction:

   int program(int X) { return <expression>; }

   Follows cdecl calling conventions and platform-specific name mangling policy. *)

let compile_exp (ast:exp) : Cunit.cunit =
  let block_name = (Platform.decorate_cdecl "program") in
	let code : insn list = List.rev (Ret :: (emit_exp ast [])) in
		let code_block : insn_block = {global = true; label = mk_lbl_named (block_name); insns = code} in
			let blockComp : Cunit.component = Cunit.Code code_block
				in [blockComp] 

